<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class ModelProduct extends CI_Model {
		public function getAllProduct() {
			$sql = "select a.*, b.product_stock from product as a left join product_stock as b on b.product_code = a.product_code";
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0) {
				return $query->result();
			} else {
				return false;
			}
		}

		public function getProductByCode($product_code) {
			$sql = "select a.*, b.product_stock from product as a left join product_stock as b on b.product_code = a.product_code where a.product_code = '".$product_code."'";
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0) {
				return $query->result();
			} else {
				return false;
			}
		}

		public function mutasiStockByProduct($product_code) {
			$sql = "select sum(mutasi_total) as jumlah FROM mutasi_stock where product_code = '".$product_code."'";
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0) {
				$rows	= $query->row();
				$jumlah  = $rows->jumlah;

				return $jumlah;
			} else {
				return false;
			}
		}
	}
?>
