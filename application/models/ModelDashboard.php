<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class ModelDashboard extends CI_Model {
		public function getDataStrSipExpired() {
			$sql = "select a.*, b.emp_full_name, b.emp_dob, c.job_name, datediff(a.strsip_end_date, current_date()) as remaining_day from t_strsip as a left join t_m_employee as b on 
					b.emp_id = a.emp_id left join t_m_job as c on c.job_id = b.emp_job_position where datediff(a.strsip_end_date, current_date()) < 180 and a.strsip_status = '0'";
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0) {
				return $query->result();
			} else {
				return false;
			}
		}

		public function getDataCertificateExpired() {
			$sql = "select a.*, b.cert_name, c.emp_full_name, d.job_name from t_certificate_employee as a left join t_certificate as b on b.cert_id = a.cert_id left join t_m_employee 
					as c on c.emp_id = a.emp_id left join t_m_job as d on d.job_id = c.emp_job_position where b.cert_validity_period = 'non_lifetime_validity' and b.cert_end_date_period 
					not in('0000-00-00') and datediff(b.cert_end_date_period, current_date()) <= 180 and b.cert_status = '0'";
			// $sql = "select a.*, b.emp_full_name, c.job_name from t_certificate as a left join t_m_employee as b on b.emp_id = a.emp_id left join t_m_job as c 
			// 		on c.job_id = b.emp_job_position where a.cert_validity_period = 'non_lifetime_validity' and a.cert_end_date_period not in('0000-00-00') and 
			// 		datediff(a.cert_end_date_period, current_date()) <= 180 and a.cert_status = '0'";
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0) {
				return $query->result();
			} else {
				return false;
			}
		}

		public function getDataExpEmployeeContract() {
			$sql = "select a.*, b.unit_name, c.job_name from t_m_employee as a left join t_m_unit as b on b.unit_id = a.emp_unit left join t_m_job as c on c.job_id = a.emp_job_position 
					where a.emp_employment_status = '2'";
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0) {
				return $query->result();
			} else {
				return false;
			}
		}

		public function getTotalCertificateExpired3Month() {
			$sql = "select count(emp_id) from t_certificate where cert_validity_period = 'non_lifetime_validity' and cert_end_date_period not in('0000-00-00') and 
					(datediff(cert_end_date_period, current_date()) < 90 and cert_status = '0'";
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0) {
				return $query->result();
			} else {
				return false;
			}
		}

		public function getTotalCertificateExpired6Month() {
			$sql = "select count(emp_id) from t_certificate where cert_validity_period = 'non_lifetime_validity' and cert_end_date_period not in('0000-00-00') and 
					(datediff(cert_end_date_period, current_date()) < 180 and cert_status = '0'";
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0) {
				return $query->result();
			} else {
				return false;
			}
		}

		public function getDataCertificateExpiredEmail3month() {
			$sql = "select a.*, b.emp_full_name, c.job_name from t_certificate as a left join t_m_employee as b on b.emp_id = a.emp_id left join t_m_job as c on 
					c.job_id = b.emp_job_position where a.cert_validity_period = 'non_lifetime_validity' and a.cert_end_date_period not in('0000-00-00') and 
					(datediff(a.cert_end_date_period, current_date()) < 90 and a.cert_status = '0'";
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0) {
				return $query->result();
			} else {
				return false;
			}
		}

		public function getDataCertificateExpiredEmail6month() {
			$sql = "select a.*, b.emp_full_name, c.job_name from t_certificate as a left join t_m_employee as b on b.emp_id = a.emp_id left join t_m_job as c on 
					c.job_id = b.emp_job_position where a.cert_validity_period = 'non_lifetime_validity' and a.cert_end_date_period not in('0000-00-00') and 
					(datediff(a.cert_end_date_period, current_date()) < 180 and a.cert_status = '0'";
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0) {
				return $query->result();
			} else {
				return false;
			}
		}
	}
?>
