<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class ModelCart extends CI_Model {
		public function getOrderHeaderID() {
            $sql = "select max(order_id) as maxID FROM order_header";
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0) {
				$rows	= $query->row();
				$maxID  = $rows->maxID;
				$getNumberID = (int) substr($maxID, 12, 5);
				$getNumberID = $getNumberID + 1;

				$code = "INV/".date("m")."/".date("Y")."/";
				$order_id = $code . sprintf("%05s", $getNumberID);

				return $order_id;
			} else {
				return false;
			}
		}

		public function getAllProduct() {
			$sql = "select a.*, b.product_stock from product as a left join product_stock as b on b.product_code = a.product_code";
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0) {
				return $query->result();
			} else {
				return false;
			}
		}

		public function getProductByCode($product_code) {
			$sql = "select a.*, b.product_stock from product as a left join product_stock as b on b.product_code = a.product_code where a.product_code = '".$product_code."'";
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0) {
				return $query->result();
			} else {
				return false;
			}
		}

		public function saveMutasi($order_id, $order_date, $product_code, $qty) {
			$sql = "insert into mutasi_stock (mutasi_date, product_code, order_id, mutasi_type, mutasi_total) values ('".$order_date."', '".$product_code."', '".$order_id."', 'O', 
					'".$qty."')";
			$query = $this->db->query($sql);
			if ($query) {
				return true;
			} else {
				return false;
			}
		}

		public function saveDetailOrder($order_id, $product_code, $qty, $price, $subtotal) {
			$sql = "insert into order_detail (order_header_id, product_code, qty, price, subtotal) values ('".$order_id."', '".$product_code."', '".$qty."', '".$price."', '".$subtotal."')";
			$query = $this->db->query($sql);
			if ($query) {
				return true;
			} else {
				return false;
			}
		}

		public function saveMasterOrder($order_id, $order_date, $customer_id, $promo_code, $amount_discount, $subtotal, $ppn, $total) {
			$sql = "insert into order_header (order_id, order_date, customer_id, promo_code, discount_amount, nett, ppn, total) values ('".$order_id."', '".$order_date."', 
					'".$customer_id."', '".$promo_code."', '".$amount_discount."', '".$subtotal."', '".$ppn."', '".$total."')";
			$query = $this->db->query($sql);
			if ($query) {
				return true;
			} else {
				return false;
			}
		}
	}
?>
