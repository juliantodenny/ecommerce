<html>
    <head></head>
    <body>
        <table>
            <thead>
                <tr>
                    <th colspan="5">Your Cart</th>
                </tr>
                <tr>
                    <th>No.</th>
                    <th>Product Name</th>
                    <th>Product Price</th>
                    <th>Product Qty</th>
                    <th>Discount</th>
                    <th>Total</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php echo $listCart; ?>
            </tbody>
        </table>
    </body>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.3/jquery.min.js" integrity="sha512-STof4xm1wgkfm7heWqFJVn58Hm3EtS31XFaagaa8VMReCXAkQnJZ+jEy8PCC/iT18dFy95WcExNHFTqLyp72eQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        function deleteCart(rowid) {
            $.ajax({
                type: 'post',
                url: '<?php echo base_url();?>cart/deletecart',
                data: {rowid:rowid},
                success: function (data) {
                    if (data == "success-delete-cart") {
                        alert("Success to delete product in cart");
                        window.location.reload(true);
                    } else {
                        alert("Failed to delete product in cart");
                    }
                    
                }
            });
        }

        function updateCart(rowid) {
            $.ajax({
                type: 'post',
                url: '<?php echo base_url();?>cart/deletecart',
                data: {rowid:rowid},
                success: function (data) {
                    if (data == "success-delete-cart") {
                        alert("Success to delete product in cart");
                        window.location.reload(true);
                    } else {
                        alert("Failed to delete product in cart");
                    }
                    
                }
            });
        }

        function checkoutCart() {
            $.ajax({
                type: 'post',
                url: '<?php echo base_url();?>cart/checkoutcart',
                data: {},
                success: function (data) {
                    if (data == "success-checkout") {
                        alert("Success to checkout your shopping");
                        window.location.href="<?php echo base_url();?>product";
                    } else if (data == "failed-save-master-order") {
                        alert("Failed to save master order");
                        return;
                    } else {
                        alert("Failed to save detail order");
                        return;
                    }
                }
            });
        }
    </script>
</html>