<html>
    <head></head>
    <body>
        <table>
            <thead>
                <tr>
                    <th colspan="4">Product</th>
                    <td><a href="<?php echo base_url();?>cart">Cart</a></td>
                </tr>
                <tr>
                    <th>No.</th>
                    <th>Product Name</th>
                    <th>Product Stock</th>
                    <th>Product Price</th>
                    <th>Qty</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php echo $listProduct; ?>
            </tbody>
        </table>
    </body>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.3/jquery.min.js" integrity="sha512-STof4xm1wgkfm7heWqFJVn58Hm3EtS31XFaagaa8VMReCXAkQnJZ+jEy8PCC/iT18dFy95WcExNHFTqLyp72eQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        function addToCart(product_code) {
            var product_stock = $("#product_stock_"+product_code).val();
            var product_qty = $("#product_qty_"+product_code).val();

            if (product_qty == "") {
                alert("Fill quantity to buy");
                return;
            }

            if (product_stock < product_qty) {
                alert("Product stock is not enough");
                return;
            }

            $.ajax({
                type: 'post',
                url: '<?php echo base_url();?>product/addproduct',
                data: {product_code:product_code, product_qty:product_qty},
                success: function (data) {
                    alert("Success to add product to cart");
                    window.location.reload(true);
                    // window.location.href="<?php echo base_url();?>cart";
                }
            });
        }
    </script>
</html>