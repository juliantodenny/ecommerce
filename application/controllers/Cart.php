<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');

class Cart extends CI_Controller {
	public function __construct() {
        parent::__construct();
		if (!$this->session->userdata('session_userid') || $this->session->userdata('session_userid') == NULL) {
			redirect('auth');
		}
		$this->load->library('cart');
    }
	
	public function index() {
		$data["listCart"] = "";
		$no = 0;
		$order_total = 0;
		$total = 0;
		if ($this->cart->contents()) {
			foreach ($this->cart->contents() as $row) {
				$no++;
				$total = $row['price']*$row['qty'];
				$id = $row['rowid'];

				if ($row["id"] == "010001" && $row["qty"] > 0) {
					$discount = "Discount Applied<br><small>Setiap pembelian MIRANDA H.C N.BLACK 30.MC1, mendapat potongan Rp. 1.000</small>";
					$total = $total-1000;
				} else {
					$discount = "-";
					$total = $total;
				}

				$order_total += $total;

				$data["listCart"] .= "<tr>
										<td>".$no.".</td>
										<td>".$row['name']."</td>
										<td>Rp. ".number_format($row['price'])."</td>
										<td><input type='number' id='product_qty' name='product_qty' value='".$row['qty']."'></td>
										<td>".$discount."</td>
										<td>Rp ".number_format($total)."</td>
										<td><button onclick='deleteCart(\"$id\")'>Delete</button></td>
									  </tr>";
			}
			$data["listCart"] .= "<tfoot>
									<tr>
										<td colspan='3'>Order Total: Rp. ".number_format($order_total)."</td>
										<td><button onclick='updateCart()'>Update Cart</button></td>
										<td><button onclick='checkoutCart()'>Checkout</button></td>
								  	</tr>
								  </tfoot>";
		} else {
			$data["listCart"] .= "";
		}

		$this->load->view('cart/index.php', $data);
	}

	public function deleteCart() {
		$rowid = $this->input->post("rowid");
		
		if ($rowid == "all") {
			$deleteCart = $this->cart->destroy();
		} else {
			$arr = array(
				'rowid' => $rowid,
				'qty' => 0
			);

			$deleteCart = $this->cart->update($arr);
		}

		if ($deleteCart == true) {
			echo "success-delete-cart";
			return;
		} else {
			echo "failed-delete-cart";
			return;
		}
	}

	public function checkoutCart() {
		$order_id = $this->ModelCart->getOrderHeaderID();
		$order_date = date("Y-m-d");
		$customer_id = "1";
		$promo_code = "pmo-001";
		$amount_discount = "1000";

		if ($this->cart->contents()) {
			foreach ($this->cart->contents() as $row) {
				$qty = $row["qty"];
				$price = $row["price"];
				$product_code = $row["id"];

				$subtotal = $qty*$price;

				$this->ModelCart->saveMutasi($order_id, $order_date, $product_code, $qty);
				$saveDetailOrder = $this->ModelCart->saveDetailOrder($order_id, $product_code, $qty, $price, $subtotal);
			}
			$price_after_discount = $subtotal-$amount_discount;
			$ppn = (10/100)*$price_after_discount;
			$total = $price_after_discount+$ppn;

			if ($saveDetailOrder == true) {
				$saveMasterOrder = $this->ModelCart->saveMasterOrder($order_id, $order_date, $customer_id, $promo_code, $amount_discount, $subtotal, $ppn, $total);
				if ($saveMasterOrder == true) {
					$this->cart->destroy();
					echo "success-checkout";
					return;
				} else {
					echo "failed-save-master-order";
					return;
				}
			} else {
				echo "failed-save-detail-order";
				return;
			}
		}
	}
}