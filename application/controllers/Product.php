<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');

class Product extends CI_Controller {
	public function __construct() {
        parent::__construct();
		if (!$this->session->userdata('session_userid') || $this->session->userdata('session_userid') == NULL) {
			redirect('auth');
		}
		$this->load->library('cart');
    }
	
	public function index() {
		$data["listProduct"] = "";
		$no = 0;
		$getAllProduct = $this->ModelProduct->getAllProduct();
		if ($getAllProduct != false) {
			foreach ($getAllProduct as $row) {
				$no++;

				$mutasiStockByProduct = $this->ModelProduct->mutasiStockByProduct($row->product_code);

				$productStock = $row->product_stock - $mutasiStockByProduct;

				$data["listProduct"] .= "<tr>
											<td>".$no.".</td>
											<td>".$row->product_name."</td>
											<td>
												<input type='hidden' id='product_stock_$row->product_code' name='product_stock_$row->product_code' value='".$productStock."'>
												".$productStock."
											</td>
											<td>Rp. ".number_format($row->product_price)."</td>
											<td><input type='number' id='product_qty_$row->product_code' name='product_qty'></td>
											<td><button onclick='addToCart(\"$row->product_code\")'>Add To Card</button></td>
										 </tr>";
			}
		} else {
			$data["listProduct"] .= "";
		}

		$this->load->view('product/index.php', $data);
	}

	public function addProduct() {
		$product_code = $this->input->post("product_code");
		$getProductByCode = $this->ModelProduct->getProductByCode($product_code);
		if ($getProductByCode != false) {
			foreach ($getProductByCode as $row) {
				$product_code = $row->product_code;
				$product_name = $row->product_name;
				$product_price = $row->product_price;
			}
		}

		$product_name = str_replace(".", "-", $product_name);
		$product_name = preg_replace('/[^A-Za-z0-9\-]/', '', $product_name);

		$arr = array(
			'id' => $product_code,
			'qty' => $this->input->post("product_qty"),
			'price' => $product_price,
			'name' => $product_name
		);

        $this->cart->insert($arr);
	}
}