    <?php defined('BASEPATH') OR exit('No direct script access allowed');
    
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;
    
    class Mailer {    
        protected $_ci;    
        protected $email_pengirim = 'hr.chcgc@gmail.com'; // Isikan dengan email pengirim    
        protected $nama_pengirim = 'HRD Ciputra Hospital Citra Garden City'; // Isikan dengan nama pengirim    
        protected $password = 'Cihos123!'; // Isikan dengan password email pengirim    
        
        public function __construct(){        
            $this->_ci = &get_instance();
            require_once(APPPATH.'third_party/PHPMailer/src/Exception.php');
            require_once(APPPATH.'third_party/PHPMailer/src/PHPMailer.php');
            require_once(APPPATH.'third_party/PHPMailer/src/SMTP.php');    
        }    
        
        public function send($data, $cc_email){        
            $mail = new PHPMailer;
            $mail->isSMTP();
            $mail->Host = 'ssl://smtp.gmail.com';
            $mail->Username = "hr.chcgc@gmail.com";
            $mail->Password = "pmkdurrvapxynxty";
            $mail->Port = 465;        
            $mail->SMTPAuth = true;        
            $mail->SMTPSecure = 'ssl';        
            $mail->SMTPDebug = 2; // Aktifkan untuk melakukan debugging        
            
            $mail->setFrom($this->email_pengirim, $this->nama_pengirim);  
            
            foreach ($cc_email as $row) {
                $mail->AddCC($row->emp_email, $row->emp_full_name);
            }
            
            // $mail->addCC();        
            $mail->addAddress($data['email_penerima'], '');             
            $mail->Subject = $data['subjek'];        
            $mail->isHTML(true);          
            $mail->Body = $data['content'];          
            
            $send = $mail->send();        
            if($send){
                $response = array('status'=>'Sukses', 'message'=>'Email berhasil dikirim');        
            }else{
                $response = array('status'=>'Gagal', 'message'=>'Email gagal dikirim');        
            }        
            
            return $response;    
        }    
        
        public function send_with_attachment($data){        
            $mail = new PHPMailer;        
            $mail->isSMTP();        
            $mail->Host = 'smtp.gmail.com';        
            $mail->Username = $this->email_pengirim; // Email Pengirim        
            $mail->Password = $this->password; // Isikan dengan Password email pengirim        
            $mail->Port = 465;        
            $mail->SMTPAuth = true;        
            $mail->SMTPSecure = 'ssl';        
            // $mail->SMTPDebug = 2; // Aktifkan untuk melakukan debugging        
            
            $mail->setFrom($this->email_pengirim, $this->nama_pengirim);        
            $mail->addAddress($data['email_penerima'], '');        
            $mail->isHTML(true); // Aktifkan jika isi emailnya berupa html        
            $mail->Subject = $data['subjek'];        
            $mail->Body = $data['content'];        
            $mail->AddEmbeddedImage('image/logo.png', 'logo_mynotescode', 'logo.png'); // Aktifkan jika ingin menampilkan gambar dalam email        
            
            if($data['attachment']['size'] <= 25000000){ // Jika ukuran file <= 25 MB (25.000.000 bytes)            
                $mail->addAttachment($data['attachment']['tmp_name'], $data['attachment']['name']);            
                $send = $mail->send();            
                if($send){ // Jika Email berhasil dikirim                
                    $response = array('status'=>'Sukses', 'message'=>'Email berhasil dikirim');            
                }else{ // Jika Email Gagal dikirim               
                    $response = array('status'=>'Gagal', 'message'=>'Email gagal dikirim');            
                }        
            }else{ // Jika Ukuran file lebih dari 25 MB            
                $response = array('status'=>'Gagal', 'message'=>'Ukuran file attachment maksimal 25 MB');        
            }        
            
            return $response;    
        }
    }